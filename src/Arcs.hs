module Arcs
  (arcs
  )where

import Data.List


e :: Float
e = 2.71828


phi :: Float -> Float
phi x = e ** (-0.5 * x ** 2) / sqrt (2 * pi)


general :: Float -> Float -> Float -> Float
general mean stddev x = stretch * phi ((x - mean) / stddev)
  where stretch = 1.0 / stddev


noZeros :: (Num a, Eq a, Ord a) => [a] -> [a]
noZeros = takeWhile (> 0) . dropWhile (== 0)


generateArc :: Float -> Float -> [Int]
generateArc peak x = noZeros $ map scale $ vals
  where vals = map (general peak x) [lo, lo + 0.1..hi]
        lo = peak / 2.0
        hi = peak + lo
        max = maximum vals
        scale = floor . (* (peak / max))


rotate :: [a] -> [a]
rotate xs = drop (l - 1) xs ++ take (l - 1) xs
  where l = length xs


arcOneDist :: [Int]
arcOneDist = (rotate halfArc) ++ halfArc
  where halfArc = generateArc 8 0.196142        


arcTwoDist :: [Int]
arcTwoDist = generateArc 16 0.33974
        

arcs :: [(Int, Int)]
arcs = zip arcOneDist arcTwoDist
