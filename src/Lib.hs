module Lib where

import qualified Data.Set as Set
import Data.List (foldl')
import Data.Random
import Data.Random.List
import qualified Data.Map.Strict as Map
import Control.Monad

type Coord = (Float, Float)

data Performer =
  Performer
  { performerLoc :: Coord
  , performerReach :: Float
  }
  deriving (Show, Eq, Ord, Read)

type Performers = Map.Map Int Performer

data Action =
  Exist
  | ExistOnChain RopeId
  | Speak String
  | Pass
  | Stop
  deriving (Show, Read, Eq)

data PerformerAction =
  Action
  { actionTarget :: Int
  , actionLocation :: Coord
  , actionType :: Action
  }
  deriving (Show, Read, Eq)

type PerformerActions = [PerformerAction]

type RopeId = String

data Rope =
  Rope
  { ropeColor :: Color
  , ropeHeadPos :: Coord
  , ropeLength :: Float
  }
  deriving (Show, Read, Eq)

data Color = Red | Blue
  deriving (Show, Read, Eq)

type Chain = [(Int, Performer)]

data SpaceBounds =
  Bounds
  { boundsOrigin :: Coord
  , boundsXLength :: Float
  , boundsYLength :: Float
  }
  deriving (Show, Read, Eq)

maxPerformerReach, minPerformerReach :: Float
maxPerformerReach = 3.0
minPerformerReach = 0.1

generatePerformers :: MonadRandom m => Int -> SpaceBounds -> m Performers
generatePerformers performerCount bounds =
  go performerCount bounds Map.empty
  where go 0 b performersSoFar = return performersSoFar
        go n b performersSoFar = do
          newPerformer <- drawUniquePerformer b performersSoFar
          go (n - 1) b $ Map.insert n newPerformer performersSoFar

drawUniquePerformer :: MonadRandom m => SpaceBounds -> Performers -> m Performer
drawUniquePerformer bounds existing = do
  newPerformerCoord <- drawFromBounds bounds
  if newPerformerCoord `elem` coordsOf existing
  then drawUniquePerformer bounds existing
  else do
    newPerformerReach <- randomReach
    return $ Performer newPerformerCoord newPerformerReach

drawFromBounds :: MonadRandom m => SpaceBounds -> m Coord
drawFromBounds bounds = runRVar selectPoint StdRandom
  where selectPoint = do
          x <- uncurry uniform $ xRange bounds
          y <- uncurry uniform $ yRange bounds
          return (x, y)

xRange :: SpaceBounds -> (Float, Float)
xRange bounds = (xLo, xHi)
  where (xLo, _) = boundsOrigin bounds
        xHi = boundsXLength bounds + xLo

yRange :: SpaceBounds -> (Float, Float)
yRange bounds = (yLo, yHi)
  where (yLo, _) = boundsOrigin bounds
        yHi = boundsYLength bounds + yLo

coordsOf :: Performers -> Set.Set Coord
coordsOf performers =
  Set.fromList $ Prelude.map performerLoc $ Map.elems performers

randomReach :: MonadRandom m => m Float
randomReach = runRVar dist StdRandom
  where dist = uniform minPerformerReach maxPerformerReach

performerActions :: Performers -> Rope -> Chain -> PerformerActions
performerActions ps r c = Prelude.map getAction $ Map.toList ps
  where getAction idP = actionForPerformer r c idP

actionForPerformer :: Rope -> Chain -> (Int, Performer) -> PerformerAction
actionForPerformer r c pId@(id, p)
  | pId `inChain` c = actionForRopePosition $ ropePosition c pId
  | otherwise = Action id (performerLoc p) Exist

inChain :: (Int, Performer) -> Chain -> Bool
inChain p c = p `elem` c

ropePosition :: Chain -> (Int, Performer) -> Float
ropePosition chain pId =
  let (init : performers) = map snd $ takeWhile (not . (== pId)) chain
  in fst $ Data.List.foldl' buildDistance (0.0, init) $ performers
  where buildDistance (curDist, (Performer c1 _)) p2@(Performer c2 _) =
          (curDist + euclidean c1 c2, p2)

actionForRopePosition :: Float -> PerformerAction
actionForRopePosition n
  | 0 < n < 10 = undefined

euclidean :: Coord -> Coord -> Float
euclidean (x1, y1) (x2, y2) = sqrt $ xDist ** 2 + yDist ** 2
  where xDist = x2 - x1
        yDist = y2 - y1

renderActions :: PerformerActions -> IO ()
renderActions pas =
  mapM_ renderAction pas

renderAction :: PerformerAction -> IO ()
renderAction _ = undefined

resolvePerformers :: PerformerActions -> Performers -> Performers
resolvePerformers pas ps = undefined

resolveChain :: PerformerActions -> Chain -> Chain
resolveChain pas c = undefined

redRope = Rope Red (0, 0) 100.0

main :: IO ()
main = do
  let bounds = Bounds (0.0, 0.0) 100.0 100.0
  performers <- generatePerformers 20 bounds
  chainHead <- runRVar (randomElement $ Map.toList performers) StdRandom
  let chain = [chainHead]
  performanceLoop performers redRope chain

performanceLoop :: Performers -> Rope -> Chain -> IO ()
performanceLoop ps r c = go ps c
  where go ps c = do
          let c' = growChain c ps
          let actions = performerActions ps r c'
          let ps' = resolvePerformers actions ps
          let c'' = resolveChain actions c''
          renderActions actions
          go ps' c''

growChain :: Chain -> Performers -> Chain
growChain c ps = undefined
