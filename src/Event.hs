{-# LANGUAGE GADTs #-}

module Event where

import Prelude hiding (Word)

data Event where
  Introspective :: IntrospectiveCommand -> Event
  Responsive :: ResponsiveCommand -> Event

type ResponsiveCommand = ResponsiveAction -> Count -> Volume

type IntrospectiveCommand = IntrospectiveAction -> Count -> Volume

data Count = Once | Repeatedly
  deriving (Eq, Show)

data IntrospectiveAction = Speak Word | Sing | Hum
  deriving (Eq, Show)

data ResponsiveAction = Imitate
  deriving (Eq, Show)

data Volume = Loudly | Moderately
  deriving (Eq, Show, Ord)

type Word = String
